# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2018-2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np
import math3d as m3d

from pymoco.kinematics import joints
from pymoco.robots.robot_definition import RobotDefinition


class Panda(RobotDefinition):

    def __init__(self, **kwargs):
        RobotDefinition.__init__(self, **kwargs)

        self._dof = 7

        # Joint position limits
        self._pos_lim_act_data_sheet = np.deg2rad(np.array(
            [[-166, 166],
             [-101, 101],
             [-166, 166],
             [-176, -4],
             [-166, 166],
             [-1, 215],
             [-166, 166]],
            dtype=np.double)).T
        self._pos_act_centres = self._pos_lim_act_data_sheet.mean(axis=0)
        self._pos_lim_act = (
            0.99 * (self._pos_lim_act_data_sheet - self._pos_act_centres)
            + self._pos_act_centres)
        
        # Joint speed limits
        self._spd_lim_act = np.deg2rad(np.array(
            [150, 150, 150, 150, 180, 180, 180],
            dtype=np.double)).T

        # Home pose
        self._q_home = np.zeros(self._dof, dtype=np.float64)
        # self._q_home[4] = -np.pi/2
        # self._q_home[6] = np.pi/2

        # Internal link transform for the eight robot parts.
        self._link_xforms = [m3d.Transform() for i in range(self._dof + 1)]

        self._link_xforms[0].pos.z = 0.333  # 0.1419

        self._link_xforms[1].pos.z = 0  # - self._link_xforms[0].pos.z
        self._link_xforms[1].orient.rotate_xb(-np.pi/2)

        self._link_xforms[2].pos.y = -0.316
        self._link_xforms[2].orient.rotate_xb(np.pi/2)

        self._link_xforms[3].pos.x = 0.0825
        self._link_xforms[3].orient.rotate_xb(np.pi/2)

        self._link_xforms[4].pos.x = -0.0825
        self._link_xforms[4].pos.y = 0.384
        self._link_xforms[4].orient.rotate_xb(-np.pi/2)

        self._link_xforms[5].orient.rotate_xb(np.pi/2)

        self._link_xforms[6].pos.x = 0.088
        self._link_xforms[6].orient.rotate_xb(np.pi/2)

        self._link_xforms[7].pos.z = 0.107

        self._joint_xforms = self.joint_xforms
        
    def get_joint_xforms(self):
        return [joints.RevoluteJoint() for i in range(self._dof)]
    joint_xforms = property(get_joint_xforms)
