# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2018-2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import os
import time

import numpy as np
import bpy
import pymoco

rob = pymoco.robots.emika.Emika()
fc = pymoco.kinematics.frame_computer.FrameComputer(rob_def=rob,
                                                    cache_in_frames=True)

links = []
link_name_tpl = 'link{}_collision'

def load_links():
    global links
    robdir = os.path.dirname(pymoco.robots.emika.__file__)
    linkdir = os.path.join(os.path.split(robdir)[0], 'simulator')
    # link_name_tpl = 'link{}.STL'
    for i in range(8):
        ln = link_name_tpl.format(i)
        bln = ln.replace('_', ' ')
        if bln in bpy.data.objects:
            print('Link {} already loaded'.format(ln))
        else:
            print('Loading link {}'.format(ln))
            bpy.ops.import_mesh.stl(filepath=os.path.join(linkdir, ln + '.STL'))
        links.append(bpy.data.objects[bln])
    return


def pose_links():
    global links
    if len(links) == 0:
        load_links()
    itrfs = fc.get_in_frame(array=True)
    for link, itrf in zip(links, itrfs):
        link.matrix_world = itrf.T
    linktf = bpy.data.objects['linktf']
    linktf.matrix_world = fc.tool_pose_array.T
    return

    
def set_joint_pose(jpos):
    fc.joint_angles_vec = jpos
    pose_links()
    return


def cosine(T=5.0, sleep=0.01):
    w = 2.0
    a = 0.25
    q0 = fc.joint_angles_vec
    q = q0.copy()
    t0 = time.time()
    while time.time()-t0 < T:
        q[3] = q0[3] - a * (1 - np.cos(w * (time.time()-t0)))
        set_joint_pose(q)
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        # time.sleep(sleep_time)


"""
Run this in the console:
import emika_poser as ep
ep.cosine()
"""
