# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2018-2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import threading
import socket
import struct
import time
import enum

import numpy as np
import math3d as m3d

from .. import robots
from . import RobotFacade
from .. import kinematics


class _Packet:

    # Packet state and type constants
    class Type(enum.IntEnum):
        NONE = 0
        CLIENT = 1
        SERVER = 2
        REJECTED = 3
        QUIT = 4
        INIT = 5
        ACK = 6
        CONN = 7

    # _Packet struct: Cycle ID, State, Type, Joint Pos Act, Joint Pos Cmd
    struct = struct.Struct('<BBB7d7d16d')

    n_data = 3 + 7 + 7 + 16
    
    def __init__(self, *args):
        if len(args) == 0:
            self.data = self.n_data * [0]
        elif len(args) == 1 and type(args[0]) == bytes:
            self.bytes = args[0]
        elif len(args) == self.n_data:
            self.data = args
        else:
            raise Exception('_Packet.__init__: ' +
                            'Need 0, 1 or 17 arguments for initialization.')

    def __repr__(self):
        return f'cid:{self.cycle_id} state:{self.state} type:{self.type}'

    def get_data(self):
        return ([self.cycle_id, self.state, self.type] +
                list(self.joint_positions_act) +
                list(self.joint_positions_cmd) +
                list(self.flange_pose))

    def set_data(self, data):
        self.cycle_id = data[0]
        self.state = data[1]
        self.type = data[2]
        self.joint_positions_act = list(data[3:3+7])
        self.joint_positions_cmd = list(data[3+7:3+7+7])
        self.flange_pose = list(data[3+7+7:3+7+7+16])

    data = property(get_data, set_data)

    def get_bytes(self):
        return self.struct.pack(*self.data)

    def set_bytes(self, bytes):
        data = self.struct.unpack(bytes)
        self.data = data

    bytes = property(get_bytes, set_bytes)


class FMSFacade(RobotFacade, threading.Thread):

    def __init__(self, **kwargs):
        self._log_level = kwargs.get('log_level', 2)
        RobotFacade.__init__(self, **kwargs)
        self._rob_def = robots.get_robot_type(kwargs['rob_type'])()
        self._frame_computer = kinematics.FrameComputer(
            rob_def=self._rob_def)
        self._fms_host = kwargs.get('rob_host')
        self._fms_port = kwargs.get('rob_port')
        self._fms_addr = (self._fms_host, self._fms_port)
        # print(self._fms_addr)
        threading.Thread.__init__(self, name=repr(self))
        self.daemon = True
        self.__stop = False
        self._fms_sock = socket.socket(type=socket.SOCK_DGRAM)
        self._packet_counter = 0
        self._cmdp = _Packet()
        self._cmdp.type = _Packet.Type.CLIENT
        # Prepare a quit packet
        self._quitp = _Packet()
        self._quitp.type = _Packet.Type.QUIT
        self._t_packet = None
        self._t_pll = None
        self._cycle_time = self._cycle_time_nom = kwargs.get('cycle_time',
                                                             0.01)

    def __repr__(self):
        return f'{self.__class__.__name__}<{self._fms_host}:{self._fms_port}>'

    def get_current_arrival_time(self):
        """Return the time at which the current, i.e. latest, status
        packet was received from the robot controller."""
        return self._t_pll

    current_arrival_time = property(get_current_arrival_time)

    def set_cmd_joint_pos(self, cmd_jpos):
        jpos_vio = self.check_joint_pos_limits(cmd_jpos)
        if np.any(jpos_vio):
            raise RobotFacade.JointLimitViolationError(jpos_vio)
        # if np.any(jpos_vio[0]):
        #     raise RobotFacade.JointLimitViolationError(
        #         'Lower joint limit violation on joint' +
        #         f'{np.where(jpos_vio[0])[0][0]}!')
        # if np.any(jpos_vio[1]):
        #     raise RobotFacade.JointLimitViolationError(
        #         'Upper joint limit violation on joint' +
        #         f' {np.where(jpos_vio[1])[0][0]}!')
        self._q_increment = cmd_jpos - self._q_tracked
        self._q_tracked = cmd_jpos.copy()
        self._cmdp.joint_positions_cmd = self._q_tracked
        self._fms_sock.sendto(self._cmdp.bytes, self._fms_addr)
        with self._control_cond:
            self._control_cond.notify_all()

    cmd_joint_pos = property(RobotFacade.get_cmd_joint_pos,
                             set_cmd_joint_pos)

    def stop(self, join=False):
        # Send a quit packet
        self._fms_sock.sendto(self._quitp.bytes, self._fms_addr)
        self.__stop = True
        if join:
            self.join()

    def get_franke_flange_pose(self) -> m3d.Transform:
        """Get the flange pose (provided that no tool transform is configured
        in the controller) as reported by the controller via the
        franka motion service.
        """
        return self._franka_flange_pose.copy()

    def run(self):
        # Set up for interaction
        initp = _Packet()
        initp.type = _Packet.Type.INIT
        actp = _Packet()
        ackp = _Packet()
        # Initialize
        print('Initializing connection')
        self._fms_sock.sendto(initp.bytes, self._fms_addr)
        # Get ack
        ackp.bytes = self._fms_sock.recv(4096)
        if ackp.type == _Packet.Type.REJECTED:
            raise Exception('Franka facade got rejected')
        else:
            print('Accepted')
            self._q_increment = np.zeros(self._rob_def.dof)
            self._q_actual = self._q_tracked = ackp.joint_positions_act
            self._t_packet = self._t_pll = time.time()
            self._cycle_id = ackp.cycle_id
            # self._event_publisher.publish(time.time())
        while not self.__stop:
            actp.bytes = self._fms_sock.recv(4096)
            self._t_packet_last = self._t_packet
            self._t_pll_last = self._t_packet
            self._t_packet = time.time()
            self._t_pll += (self._cycle_time_nom +
                            0.01 * (self._t_packet_last - self._t_pll_last))
            self._q_actual = actp.joint_positions_act
            self._frame_computer(self._q_actual)
            # Note that transforms are reported as a 16-vector in column-major!
            self._franka_flange_pose = m3d.Transform(
                np.array(actp.flange_pose).reshape((4, 4)).T)
            self._event_publisher.publish(self._t_packet)
            self._cycle_time = (0.1 * (self._t_packet - self._t_packet_last) +
                                0.9 * self._cycle_time)
