# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2018-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import pymoco.robots
import pymoco.facades
from pymoco.robots.panda import Panda
from pymoco.facades.franka import FMSFacade


pymoco.robots.register_robot_type(Panda)
pymoco.facades.register_facade_type(FMSFacade)
